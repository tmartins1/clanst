const http = require('http');
const request = require("request");
const url = require('url');

const port = process.env.PORT || 4000

console.log("Starting the server...   ")

const server = http.createServer(function (req, resp) {
  const requestURL = url.parse(req.url, true);
  if (requestURL.pathname === '/proxy') {
    console.log('proxy to:', requestURL.query.url)
    req.pipe(request(requestURL.query.url)).pipe(resp)
  }
})

server.listen(port)
console.log('Server started!')
console.log(`  Local:            http://localhost:${port}/\n`)