import React, { Component } from 'react';
import 'styles/SearchBar.css'
import MagnifingGlass from 'assets/images/magnifying_glass.svg'

export default class SearchBar extends Component {
  state = { value: '' };

  handleChange = (event) => {
    this.setState({ value: event.target.value })
  }

  render() {
    return (
      <div className="search-bar-container">
        <img className="search-bar-image" src={MagnifingGlass} alt="Search" onClick={event => this.props.handleSubmit(event, this.state.value)} />
        <form className="search-bar-form" onSubmit={event => this.props.handleSubmit(event, this.state.value)}>
          <input className="search-bar-input" type="text" value={this.state.value} onChange={this.handleChange} />
          <button hidden type="submit">Submit</button>
        </form>
      </div>
    );
  }
}