import React, { Component } from 'react'
import SearchBar from 'components/SearchBar';
import 'styles/Players.css'
import API from 'utils/API';

export default class Players extends Component {
  state = {
    playerData: {}
  }

  handleSearch = (event, query) => {
    event.preventDefault()
    API.fetchPlayer('username')
      .then(data => {
        console.log("user data", data)
      })
  }

  render() {
    return (
      <div className="players-container">
        <SearchBar handleSubmit={this.handleSearch} />
      </div>
    )
  }
}