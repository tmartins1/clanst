import React, { Component } from 'react'
import 'styles/NavBar.css'
import NavBarSection from 'components/NavBarSection'
import { players, clans, leagues } from 'utils/constants'

export default class NavBar extends Component {

  isSelected = (value) => {
    if (this.props.value === value)
      return 'selected'
  }

  render() {
    return (
      <div className="navbar-container">
        <div className="navbar-wrapper">
          <NavBarSection text={players} onClick={this.props.onClick} selected={this.isSelected(players)} />
          <NavBarSection text={clans} onClick={this.props.onClick} selected={this.isSelected(clans)} />
          <NavBarSection text={leagues} onClick={this.props.onClick} selected={this.isSelected(leagues)} />
        </div>
      </div>
    )
  }
}