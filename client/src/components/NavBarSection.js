import React, { Component } from 'react'
import 'styles/NavBarSection.css'

export default class NavBarSection extends Component {
  constructor(props) {
    super();
    this.textRef = React.createRef();
  }

  setUnderlineColor = () => {
    if (this.props.selected)
      this.textRef.current.style.borderBottomColor = '#7B4743';
    else
      this.textRef.current.style.borderBottomColor = '#4F4B27 ';
  }

  componentDidMount() {
    this.setUnderlineColor();
  }

  componentDidUpdate() {
    this.setUnderlineColor();
  }

  render() {
    return (
      <div className="navbar-section" name={this.props.text} onClick={this.props.onClick} ref={this.textRef}>
        <p className="navbar-text">
          {this.props.text}
        </p>
      </div>
    )
  }
}
