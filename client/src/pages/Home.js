import React, { Component } from 'react'
import NavBar from 'components/NavBar';
import Players from 'components/Players';
import Clans from 'components/Clans';
import Leagues from 'components/Leagues';
import { players, clans, leagues } from 'utils/constants'
import 'styles/Home.css'

export default class Home extends Component {
  state = { currentTab: players }

  changeTab = (event) => {
    this.setState({ currentTab: event.currentTarget.getAttribute('name') });
    console.log(this.state);
  }

  displayTab = () => {
    switch (this.state.currentTab) {
      case players:
        return <Players />;

      case clans:
        return <Clans />;

      case leagues:
        return <Leagues />;

      default:
        break;
    }
  }

  render() {
    return (
      <div className="home-container">
        <NavBar onClick={this.changeTab} value={this.state.currentTab} />
        {this.displayTab()}
      </div>
    )
  }
}