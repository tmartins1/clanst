import { proxyURL } from 'utils/constants'

export default class API {
  static fetchPlayer(username) {
    const url = new URL(proxyURL);
    url.pathname = 'users'
    // url.searchParams.append('query', username)
    return this.fetch(url)
  }

  static fetch = (url) => {
    return new Promise((resolve, reject) => {
      fetch(url)
        .then(response => {
          resolve(response.json())
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}